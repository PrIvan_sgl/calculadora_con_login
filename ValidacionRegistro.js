document.addEventListener("DOMContentLoaded", function() {
    document.getElementById("formulario").addEventListener('submit', validarFormulario); 
  });
  
  function validarFormulario(evento) {
    evento.preventDefault();
    let nombre=document.getElementById("nombre").value;
    if(nombre.length == 0) {
        alert('No has escrito nada en el NOMBRE');
        return;
      }
    let apellido=document.getElementById("apellido").value;
    if(apellido.length == 0) {
        alert('No has escrito nada en el APELLIDO');
        return;
      }
    let usuario = document.getElementById('user').value;
    if(usuario.length < 4) {
      alert('El USUARIO debe tener más de 4 caracteres');
      return;
    }
    let clave = document.getElementById('password').value;
    if (clave.length < 6) {
      alert('La clave no es válida');
      return;
    }
    let motivo=document.getElementById("motivo").value;
    if(motivo.value != 1) {
        alert('No ha tildado ninguna opción en el motivo');
        return;
      }
    this.submit();
  }