$(function(){
    //VARIABLES A UTILIZAR
    var num1="0";
    var num2="0";
    var operador="";
    var resultado="";
    var mensaje="";

    //SELECCIONAMOS AMBAS PARTES DE LA PANTALLA
    var displaySuperior = $(`.display__superior`);
    var displayInferior = $(`.display__inferior`);

    //FUNCIÓN QUE ESPERA AL CLICK EN LOS BOTONES
    $(`.botonera`).on(`click`, function(){
        //ESTE SWITCH TIENE UN CASO PARA CADA BOTÓN PERSIONADO Y SELECCIONA EL ID DE DICHO BOTÓN
        switch(e.target.id){
            case `borrar`:
                //LLAMA A LA FUNCIÓN QUE VERIFICA Y BORRA DE A UN CARACTER
                borrarUno()
                break
            case `ce`:
                eliminarDisplayInferior();
                break
            case `c`:
                break
            case `dividir`:
                break
            case `7`:
                
                break
            case `8`:
                break            
            case `multiplicar`:
                break
            case `4`:
                break
            case `5`:
                break
            case `6`:
                break
            case `restar`:
                break
            case `1`:
                break
            case `2`:
                break
            case `3`:
                break
            case `sumar`:
                break
            case `0`:
                break
            case `punto`:
                break
            case `igual`:
                break
        }
    })

    //FUNCIÓN QUE DETERMINA EL VALOR MOSTRADO EN PANTALLA
    function display(){
        //CONSULTO SI NO HAY MENSAJES (EVENTOS)
        if(mensaje !==``){
            //EN ESE CASO MOSTRAMOS EL MENSAJE VACÍO Y RESTABLECEMOS LA VARIABLE COMO VACÍA
            display.displayInferior.val(mensaje);
            mensaje=``;
        //CONSULTA SI EL RESULTADO TIENE CONTENIDO Y EN CASO DE QUE SI, LO MUESTRA EN LA PANTALLA INFERIOR
        } else if(resultado!==``){
            display.Inferior.val(resultado);
        //CONSULTA SI HAY OPERADORES SELECCIONADOS, EN CASO DE QUE NO MUESTRA VACÍO EN LA PANTALLA SUPERIOR
        //Y EL VALOR DEL PRIMER NÚMERO EN LA PANTALLA INFERIOR
        }else if(operador ===``){
            displaySuperior.val(``);
            displayInferior.val(num1)
        //EN CASO DE QUE EL OPERADOR NO ESTÉ VACÍO MOSTRARÁ EN PANTALLA SUPERIOR EL PRIMER VALOR
        //Y EN PANTALLA INFERIOR EL SEGUNDO VALOR
        } else{
            displaySuperior.val(num1);
            displayInferior.val(num2);
        }
    }

    //FUNCIÓN PARA BORRAR DE A UN DÍGITO
    function borrarUno(){
        //EN CASO DE NO HABER OPERADORES BORRA UN DÍGITO AL PRIMER NÚMERO
        if(operador===``){
            num1=eliminarDigito(num1);
        //EN CASO DE SI HABER OPERADORES BORRA UN DÍGITO AL SEGUNDO NÚMERO
        } else{
            num2 = eliminarDigito(num2);
        }
    }
    //ESTA FUNCIÓN ELIMINA UN DÍGITO O UN CARACTER DEL STRING SIEMPRE QUE LA LONGITUD DEL ARRAY SEA MAYOR A 0
    //EN CASO DE SÓLO HABER UN CARACTER LO REEMPLAZA POR UN 0
    function eliminarDigito(num){
        if(num.length>1){
            return num.substring(0,num.length-1);
        }
        return `0`;
    }
    //FUNCIÓN PARA ELIMINAR EL NUMERO AGREGADO EN EL DISPLAY INFERIOR, QUE SIRVE PARA OPERAR 
    //SOBRE EL NUMERO INGRESADO ANTERIORMENTE AL OPERADOR
    function eliminarDisplayInferior() {
        if (resultado !=="") {
            resultado =="";
        }
        if (operador ==="") {
            num1 ="0";
        }else{
            num2 = "0";
        }
    }
    //FUNCIÓN PARA LIMPIAR EL DISPLAY, CORRESPONDE A C (clear) EN LA CALCULADORA.
    //SETEA TODOS LAS VARIABLES A UTILIZAR A SUS VALORES BASE
    function Limpiar() {
       num1="0";
       num2="0";
       operador= "";
       resultado=""
    }
    
    //FUNCIÓN PARA AÑADIR DIGITOS A LA OPERACIÓN
    function Añadir() {
        //AGREGAR DIGITO AL DISPLAY
        if (operador!=="") {
            num1 = agregarDigito(num1);
        } else {
            
        }
    }

    //ESTA FUNCIÓN AGREGA UN DÍGITO O UN CARACTER DEL STRING SIEMPRE QUE LA LONGITUD DEL ARRAY SEA MAYOR A 0
    //EN CASO DE SÓLO HABER UN CARACTER LO REEMPLAZA POR UN 0
    function agregarDigito(digit){
        if(num1.length>=0){
            return num1 + digit;
        }
        return `0`;
    }

});